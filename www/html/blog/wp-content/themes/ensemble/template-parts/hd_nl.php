<div class="navbar navbar-expand-lg   navbar-ens  p-0 px-md-4 sticky-top">
    <div class="container ">
      <div class="d-flex align-items-center" ><a href="/nl/"><img src="/assets/images/EnsembleApp_logo.svg" alt="" width="113"/></a></div>
		<button  class="mobNav " type="button"><span></span><span class="dsp"></span><span></span><span class="dsp"></span><span></span><span class="dsp"></span><span></span><span class="dsp"></span><span></span></button>
		
		<div class="menuOptions">
   <ul class="navbar-nav ">
      <li class="nav-item hg ">
        <a class="nav-link" href="/nl/maak_evenement.html" id="KeyBenefits" >Belangrijkste Voordelen</a>
		  <div class="dropdown-menu secNv" aria-labelledby="KeyBenefits">
			  <div class="container px-5">
		   <ul >
			  <li><a href="/nl/maak_evenement.html" >Maak</a></li>
			   <li><a href="/nl/manage_evenement.html">Beheren</a></li>
			   <li><a href="/nl/verbinden.html">Verbinden</a></li>
			   <li><a href="/nl/ontdekken.html">Ontdekken</a></li>
			   
			  </ul>
		  </div>
		  </div>
      </li>
      <li class="nav-item hg">
        <a class="nav-link " href="/nl/prijzen.html">Prijzen</a>
      </li>
    
    <li class="nav-item hg active">
        <a class="nav-link" href="/blog/nl/">Deskundige Tips</a>
      </li>
		 <li class="nav-item hg">
        <a class="nav-link" href="/nl/overons.html">Over Ensemble</a>
		</li>
		<div class="d-flex ctas order-lg-0 order-md-1 order-sm-1 order-1">
		 <li class="nav-item">
        <a class="btn button-orange button-translate" href="/nl/demo.html">Bekijk onze demo</a>
      </li></div></ul>
  </div>

    
		
		
		 
    </div>
  </div>
	
	